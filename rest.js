var express = require('express');
var app = express();

var sistemes = [
  { nom : 'Windows', text : "Microsoft rulez"},
  { nom : 'MacosX', text : "Mola mola"},
  { nom : 'Linux', text : "Geeeek!"},
];


app.get('/', function(req, res) {
  res.json(sistemes);
});

app.get('/random', function(req, res) {
  var id = Math.floor(Math.random() * sistemes.length);
  var sis = sistemes[id];
  res.json(sis);
});

app.get('/sistema/:id', function(req, res) {
  if(sistemes.length <= req.params.id || req.params.id < 0) {
    res.statusCode = 404;
    return res.send('Error 404: Sistema no trobat');
  }  
  var sis = sistemes[req.params.id];
  res.json(sis);
});

app.listen(process.env.PORT || 3412);
